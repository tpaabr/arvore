//Arvore é um array de galhos
var arvore = []
//timer
var timer = 1
var notDone = true
//Cor
var totalNiveis = 10;
var razaoCor = 255/totalNiveis;
const lista = [];
for (let i = 0; i < totalNiveis; i++) {
    lista.push(i * razaoCor);
}
function setup(){
    createCanvas(600,600);
    //Posição
    let a = createVector(width/2, height);
    let b = createVector(width/2, height-100);

    //Construindo Tronco
    let tronco = new Galho(a, b, totalNiveis, PI/7, lista);
    arvore[0] = tronco;
}

function gerarGalhos(){
    for (let i = arvore.length - 1 ; i >= 0; i--) {
        if (arvore[i].temGalhos == false){
            novosGalhos = arvore[i].novoNivel();
            if (novosGalhos != null){
                arvore.push(novosGalhos[0]);
                arvore.push(novosGalhos[1]);
            }else{
                notDone = false;
            }
        }         
    }
}


function draw(){
    colorMode(RGB, 255);
    background(30,30,90);
    for (let i = 0; i < arvore.length; i++) {
        arvore[i].desenha();
    }

    if(frameCount % 60 == 0 && timer > 0){
        timer --;
    }

    if (timer == 0) {
        if (notDone){
            console.log(arvore[0])
            gerarGalhos();
        }
        timer = 1
    }
    
}