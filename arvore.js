//Criar cada galho da arvore
function Galho(inicio, fim, nivel, angulo, listaCores){
    
    //ponto Inicio
    this.inicio = inicio;
    //ponto Fim
    this.fim = fim;
    //Nivel, quantos niveis de galhos abaixo desse existem
    this.nivel = nivel;
    //lista de Cores
    this.listaCores = listaCores;

    //Saber se vai criar galhos ou não
    this.temGalhos = false

    this.desenha = () => {
        //Bug estranho
        if(this.nivel == listaCores.length){
            stroke(this.listaCores[listaCores.length-1], 100,70)
            line(this.inicio.x, this.inicio.y, this.fim.x, this.fim.y);
        }else{
            stroke(this.listaCores[this.nivel], 100, 70)
            line(this.inicio.x, this.inicio.y, this.fim.x, this.fim.y);
        }
    }

    this.novoNivel = () => {
        //Atualizando nivel dos galhos
        let nivelAtual = this.nivel - 1;
        let antigoFim = this.fim
        let antigoInicio = this.inicio

        if (nivelAtual == 0 ){
            return null
        }

        //Calculando novo Vetor direção 
        let dirDireita = p5.Vector.sub(antigoFim, antigoInicio);
        let dirEsquerda = p5.Vector.sub(antigoFim, antigoInicio);

        //Calculando novo vetor final esquerda e direita
        dirDireita.rotate(angulo);
        dirDireita.mult(0.8);
        let novoFimDireita = p5.Vector.add(antigoFim, dirDireita);
        dirEsquerda.rotate(-angulo);
        dirEsquerda.mult(0.8);
        let novoFimEsquerda = p5.Vector.add(antigoFim, dirEsquerda);

        //Criando novo Objeto baseado nos vetores calculados com o nivel atualizado
        var galhoDireita = new Galho(antigoFim, novoFimDireita, nivelAtual, angulo, this.listaCores);
        var galhoEsquerda = new Galho(antigoFim, novoFimEsquerda, nivelAtual, angulo, this.listaCores)

        //Se ela já criou os galhos, atualizar
        this.temGalhos = true

        return [galhoDireita, galhoEsquerda]
    }
}